<?php
// no direct access
defined( '_JEXEC' ) or die;

class plgContentPmjdevicemockup extends JPlugin
{
	/**
	 * Load the language file on instantiation. Note this is only available in Joomla 3.1 and higher.
	 * If you want to support 3.0 series you must override the constructor
	 *
	 * @var    boolean
	 * @since  3.1
	 */
	protected $autoloadLanguage = true;

	/**
	 * Plugin method with the same name as the event will be called automatically.
	 */
	 function onContentPrepare($context, &$article, &$params, $page = 0)
	 {
		/*
    * Plugin code goes here.
    * You can access database and application objects and parameters via $this->db,
    * $this->app and $this->params respectively
    */
    $canProceed = $context !== 'com_finder.indexer';

		if (!$canProceed)
		{
			return;
		}
    
    // check formating
    $regex  = '/\{pmjdevicemockup:(.*?)\}/';
    if (preg_match_all($regex,$article->text,$matches))
    {
      $deviceList = array('desktop','pad','phone');
      $doc = JFactory::getDocument();
      $doc->addStyleSheet('/plugins/content/pmjdevicemockup/css/device-mockups.min.css');
      $doc->addStyleSheet('/plugins/content/pmjdevicemockup/css/pmj-mockup.min.css');
      
      $path = JPluginHelper::getLayoutPath('content', 'pmjdevicemockup', 'default');
      $content  = file_get_contents($path);
      
      foreach ($matches[1] as $m => $match)
      {
        $devices  = explode(';',$match);
        if (count($devices) != 3)
        {
          return;
        }
        $pmjmockup  = $content;
        foreach ($devices as $device)
        {
          $device = explode('=',$device);
          if (!in_array($device[0],$deviceList))
          {
            return;
          }
          $pmjmockup  = preg_replace('/\{image'.ucfirst($device[0]).'\}/',$device[1],$pmjmockup);
        }
        $article->text  = str_replace($matches[0][$m],$pmjmockup,$article->text);
      }
    }
    
		return;
	}
}
?>