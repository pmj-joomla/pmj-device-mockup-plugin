<div class="device-composite">
  <div class="device-frame-iPad">
    <div class="device-wrapper">
      <div class="device" data-device="iPad" data-orientation="portrait" data-color="black">
        <div class="screen">
          <img class="img-fluid" src="{imagePad}" alt="Pad Preview" />
        </div>
      </div>
    </div>
  </div>
  <div class="device-frame-iMac">
    <div class="device-wrapper">
      <div class="device" data-device="iMac" data-orientation="portrait" data-color="black">
        <div class="screen">
          <img class="img-fluid" src="{imageDesktop}" alt="Desktop Preview" />
        </div>
      </div>
    </div>
  </div>
  <div class="device-frame-iPhoneX">
    <div class="device-wrapper">
      <div class="device" data-device="iPhoneX" data-orientation="portrait" data-color="black">
        <div class="screen">
          <img class="img-fluid" src="{imagePhone}" alt="Phone Preview" />
        </div>
      </div>
    </div>
  </div>
</div>