# PMJ Device Mockup Plugin

PMJ Device Mockup Plugin is a very simple Joomla plugin to - you might have guessed it - display a triple device mockup (desktop, pad, phone).  

It has been developed and tested using Joomla 3.8, so this will officially be considered the minimum version although it should work with all Joomla 3+ versions.

## Table of contents

* [Downloads](#downloads)
* [Demo](#demo)
* [How to use it](#how-to-use-it)
* [Image Size](#image-size)
* [3rd Party](#3rd-party)

## Downloads

**[Current Stable Version (1.1.3)](https://gitlab.com/pmj-joomla/pmj-device-mockup-plugin/-/archive/1.1.3/pmj-device-mockup-plugin-1.1.3.zip)**

**[Developement Version](https://gitlab.com/pmj-joomla/pmj-device-mockup-plugin/-/archive/dev/pmj-device-mockup-plugin-dev.zip)**

## Demo

You can see a working example in a production environment on [PMJ Rocks - Webdesign](https://pmj.rocks/webdesign)

**Note**: The layout has been built with Bootstrap 4 because I use the [PMJ Bootstrap Template](https://gitlab.com/pmj-joomla/pmj-bootstrap-template) but it should also work with Bootstrap 3 if you add the mx-auto definition.

![Demo Image](images/MockupDemo.jpg "Demo Image")

## How to use it

Simply put this string where you want the mockup to display:

`{pmjdevicemockup:desktop=path/to/desktop.jpg;pad=path/to/pad.jpg;phone=path/to/phone.jpg}`

## Image Size

Images need to have the following sizes or according aspect ratios:

* Desktop: 1920px x 1080px
* Pad: 768px x 1024px
* Phone: 375px x 812px

## 3rd Party

To create the mockups, I used the iMac, iPad and iPhoneX mockups provided by [pixelsign's html5-device-mockups](https://github.com/pixelsign/html5-device-mockups) and customized them slightly.
