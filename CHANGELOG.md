## Changelog

### 1.1

#### 1.1.3 [2020-08-03]

* Version 1.1.3
* **[BUG]** Design was not flexible

#### 1.1.2 [2019-11-05]

* Version 1.1.2
* Update doesn't work

#### 1.1.1 [2019-11-05]

* Version 1.1.1
* Add Bootrap 4 notification in readme

#### 1.1.0 [2019-11-05]

* Version 1.1.0
* Use everywhere except finder index
* Add method upgrade to plugin manifest
* Add demo image to readme

---
### 1.0

#### 1.0.1 [2019-11-04]

* Version 1.0.1
* Forgot to add updateserver

#### 1.0.0 [2019-11-04]

* Initial Commit